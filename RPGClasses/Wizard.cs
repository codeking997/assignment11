﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG
{
    public class Wizard : Character
    {
        public static bool hasMagicalPowers = true;

        public static string CanTalkToAnimals()
        {
            
            string canTalkToAnimalsString = "All wizards can talk to animals";
            return canTalkToAnimalsString;
        }
    }
}
